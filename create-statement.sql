CREATE DATABASE `grade_book` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `results` (
  `results_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`results_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='students result per year';
CREATE TABLE `students` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `student_code` varchar(40) NOT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table of Student Records';
CREATE TABLE `tests` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `type` enum('h','t','f') NOT NULL DEFAULT 'h',
  `weighting` float DEFAULT NULL,
  `quarter` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `year` int(11) NOT NULL,
  `void` enum('y','n') NOT NULL DEFAULT 'n',
  `uploaded_on` datetime NOT NULL,
  `calculated_grade` float NOT NULL,
  `calculated` enum('y','n') DEFAULT 'n',
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='type and quarter';
