$( document ).ready( function( ) {
	$( '#dataTable' ).dataTable( {
		"columnDefs": [
			{
				"targets": [ 5, 6 ],
				"visible": false
			}
		],
		"order": [[ 5, "asc" ], [ 6, "asc" ]] //sorts the students inline with the specification
	} );
} );