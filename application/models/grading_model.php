<?php

class Grading_model extends CI_Model
{

	// --------------------------------------------------------------------

	/**
	 * __construct( )
	 *
	 * @access public
	 */

	public function __construct( )
	{
		parent::__construct( );
	}

	// --------------------------------------------------------------------

	/**
	 * get_student( )
	 *
	 * @access public
	 * @param int
	 * @return int
	*/

	public function get_student( $studentCode )
	{
		$this->db->select( 'student_id' );
		$this->db->from( 'students' );
		$this->db->where( 'student_code', $studentCode );
		return $this->db->get( );
	}

	// --------------------------------------------------------------------

	/**
	 * create_student( )
	 *
	 * @access public
	 * @param strings
	 * @return int
	*/

	public function create_student( $firstName, $lastName, $studentCode )
	{
		$data = array(
			'first_name' => $firstName,
			'last_name' => $lastName,
			'student_code' => $studentCode
		);

		$this->db->insert( 'students', $data );

		return $this->db->insert_id( );
	}

	// --------------------------------------------------------------------

	/**
	 * insert_test_data( )
	 *
	 * @access public
	 * @param strings
	 * @return int
	*/

	public function insert_test_data( $student_id, $r, $quarter, $year )
	{
		$weighting = 100;

		if( strtolower( $r ) == 'h' ) {
			$weighting = 40;
		}

		if( strtolower( $r ) === 't' ) {
			$weighting = 60;
		}

		$data = array(
			'student_id' => $student_id,
			'type' => $r,
			'weighting' => $weighting,
			'quarter' => $quarter,
			'year' => $year,
			'uploaded_on' => date( 'Y-m-d H:i:s' )
		);

		$this->db->insert( 'tests', $data );

		return $this->db->insert_id( );
	}

	// --------------------------------------------------------------------

	/**
	 * insert_result_data( )
	 *
	 * @access public
	 * @param strings
	 * @return voids
	*/

	public function insert_result_data( $student_id, $test_id, $r )
	{

		$data = array(
			'student_id' => $student_id,
			'test_id' => $test_id,
			'value' => $r
		);

		$this->db->insert( 'results', $data );

	}

	// --------------------------------------------------------------------

	/**
	 * void_quarter_tests_by_student( )
	 *
	 * @access public
	 * @param mixed
	 * @return void
	*/

	public function void_quarter_tests_by_student( $student_id, $quarter, $year )
	{
		$data = array(
			'void' => 'y'
		);

		$this->db->where( 'student_id', $student_id );
		$this->db->where( 'quarter', $quarter );
		$this->db->where( 'year', $year );
		$this->db->update('tests', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * get_valid_tests( )
	 *
	 * @access public
	 * @param int
	 * @return mixed
	*/

	public function get_valid_tests( $quarter, $year )
	{
		$this->db->select( 'test_id, type' );
		$this->db->from( 'tests' );
		$this->db->where( 'void', 'n' );
		$this->db->where( 'calculated', 'n' );
		$this->db->where( 'quarter', $quarter );
		$this->db->where( 'year', $year );

		$query = $this->db->get( )->result( );

		return $query;
	}

		// --------------------------------------------------------------------

	/**
	 * get_existing_tests( )
	 *
	 * @access public
	 * @param int
	 * @return mixed
	*/

	public function get_existing_tests( $student_id, $r, $quarter, $year )
	{
		$this->db->select( 'test_id' );
		$this->db->from( 'tests' );
		$this->db->where( 'void', 'n' );
		$this->db->where( 'student_id', $student_id );
		$this->db->where( 'type', $r );
		$this->db->where( 'quarter', $quarter );
		$this->db->where( 'year', $year );

		$query = $this->db->get( )->result( );

		foreach( $query AS $q ) {
			return $q->test_id;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * get_results( )
	 *
	 * @access public
	 * @param int
	 * @return mixed
	*/

	public function get_results( $test_id )
	{

		$this->db->select( 'value' );
		$this->db->from( 'results' );
		$this->db->where( 'test_id', $test_id );

		$query = $this->db->get( )->result_array( );
		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * update_average_grade( )
	 *
	 * @access public
	 * @param int
	 * @return void
	*/

	public function update_average_grade( $average_grade, $test_id )
	{
		// update test record with scores
		$data = array(
			'calculated_grade' => $average_grade
		);

		$this->db->where( 'test_id', $test_id );
		$this->db->update( 'tests', $data );

	}

	// --------------------------------------------------------------------

	/**
	 * view_all_results( )
	 *
	 * @access public
	 * @param int
	 * @return mixed
	*/

	public function view_all_results(  )
	{
		$this->db->select( '*' );
		$this->db->from( 'students' );

		$students = $this->db->get( )->result( );

		foreach( $students AS $key => $value ) {
			$this->db->select( '*' );
			$this->db->from( 'tests' );
			$this->db->where('student_id', $value->student_id );
			$this->db->where( 'void', 'n' );
			$this->db->where( 'calculated', 'n' );
			$this->db->order_by( 'student_id', 'desc');

			$query = $this->db->get()->result();


			$students[$key]->results = $query;
		}

		return $students;
	}

	// --------------------------------------------------------------------

	/**
	 * insert_final_grade( )
	 *
	 * @access public
	 * @param int
	 * @return mixed
	*/

	public function insert_final_grade( $student_id, $test_quarter, $test_year, $finalGrade )
	{
		$data = array(
			'calculated' => 'y'
		);

		$this->db->where( 'student_id', $student_id );
		$this->db->where( 'quarter', $test_quarter );
		$this->db->where( 'year', $test_year );
		$this->db->where( 'void', 'n' );
		$this->db->update('tests', $data);

		$data = array(
			'student_id' => $student_id,
			'type' => 'f',
			'weighting' => 100,
			'quarter' => $test_quarter,
			'year' => $test_year,
			'uploaded_on' => date( 'Y-m-d H:i:s' ),
			'calculated_grade' => $finalGrade,
			'calculated' => 'y'
		);

		$this->db->insert( 'tests', $data );
	}

	// --------------------------------------------------------------------

	/**
	 * get_final_grades( )
	 *
	 * @access public
	 * @param int
	 * @return mixed
	*/

	public function get_final_grades(  )
	{
		$this->db->select( '*' );
		$this->db->from( 'students' );
		$this->db->join( 'tests', 'tests.student_id = students.student_id' );
		$this->db->where( 'tests.type', 'f');
		$this->db->where( 'tests.void', 'n');
		$query = $this->db->get()->result();

		return $query;
	}
	
}
?>