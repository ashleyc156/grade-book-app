<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
			<meta name="description" content="Grading System" />
			<title>Grade App - Student Grading System</title>
			<link href="/assets/css/styles.css" rel="stylesheet" />
			<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
			<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
		</head>
		<body class="sb-nav-fixed">
			<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
				<a class="navbar-brand" href="<?php echo base_url(); ?>">Grade App</a>
				<button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#">
					<i class="fas fa-bars"></i>
				</button>
			</nav>
			<div id="layoutSidenav">
				<div id="layoutSidenav_nav">
					<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
						<div class="sb-sidenav-menu">
							<div class="nav">
								<div class="sb-sidenav-menu-heading">&nbsp;</div>
								<a class="nav-link" href="<?php echo base_url(); ?>">
									<div class="sb-nav-link-icon">
										<i class="fas fa-tachometer-alt"></i>
									</div>
									Dashboard
								</a>
								<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
									<div class="sb-nav-link-icon">
										<i class="fas fa-book-open"></i>
									</div>
									Grading
									<div class="sb-sidenav-collapse-arrow">
										<i class="fas fa-angle-down"></i>
									</div>
								</a>
								<div class="collapse" id="collapsePages" aria-labelledby="headingOne" data-parent="#sidenavAccordion" style="">
									<nav class="sb-sidenav-menu-nested nav">
										<a class="nav-link" href="<?php echo base_url(); ?>grading">Upload</a>
										<a class="nav-link" href="<?php echo base_url(); ?>grading/view">View</a>
									</nav>
								</div>
							</div>
						</div>
						<div class="sb-sidenav-footer">
							<div class="small">Logged in as:</div>
							Tiffy
						</div>
					</nav>
				</div>