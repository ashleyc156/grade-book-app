<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">            
        	<div class="container-fluid">
        		<h1 class="page-header mt-4">Dashboard</h1>
				<p>Welcome to your dashboard, use the menu on your left to get started.</p>
			</div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Tiffy's Grade Book <?php echo date('Y'); ?></div>
            </div>
        </div>
    </footer>
</div>