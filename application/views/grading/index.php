<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid">
			<h1 class="mt-4">Upload</h1>
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item"><a href="/">Dashboard</a></li>
				<li class="breadcrumb-item active">Grading</li>
				<li class="breadcrumb-item active">Upload</li>
			</ol>
			<div class="container-fluid">
				<div class="header-body">
					<div class="row">
						<div class="col-xl-2 col-lg-2">
							&nbsp;
						</div>
						<div class="col-xl-8 col-lg-8">
							<?php if( isset( $submitError ) ): ?>
								<div class="alert alert-danger" role="alert">
									<p><?php echo $submitError['error']; ?></p>
								</div>									
							<?php endif; ?>
							<div class="card card-stats mb-4 mb-xl-0">
								<div class="card-body text-center">
									<?php if( isset( $uploadedTXT) ): ?>								
										<h3>You are uploading file <?php echo $uploadedTXT['upload_data']['file_name']; ?></h3>
										<p><strong>The following data will be stored and a grade average computed. If a student already has results for this period, they will be overwritten</strong></p>
									<?php else: ?>
										<?php echo form_open_multipart( base_url() . 'grading' ); ?>
											<div class="form-group">
												<label for="batch_file_upload">Upload New Grades</label>
												<input type="file" name="userfile" class="form-control-file" id="batch_file_upload" aria-describedby="uploadHelpBlock" required>
												<small id="uploadHelpBlock" class="form-text text-muted">Please only upload TXT files here.</small>
											</div>
											<div class="form-group">
												<button type="submit" id="batch_mapping_submit" class="btn btn-primary mb-2">Submit</button>
											</div>
										<?php echo form_close(); ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<div class="col-xl-2 col-lg-2">
							&nbsp;
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if( isset( $uploadedTXT ) ): ?>
			<hr />
			<?php $hidden = array('hidden_file_name' => $uploadedTXT['upload_data']['full_path'], 'hidden_file_reference' => $uploadedTXT['upload_data']['file_name']); ?>
			<?php echo form_open( base_url() . 'grading/create', 'id="batch_mapping_form"', $hidden ); ?>

				<?php
					$counter = 0;
					$maxLines = 50;
				?>

				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="card shadow">
								<div class="card-header border-0">
									<h4 class="mb-0">Uploading Grades
										<span class="form-inline float-right">
											<select name="quarter_input" class="form-control" required>
												<option value="">Please Select</option>
												<?php for($i = 1 ; $i < 5; $i++): ?>														
													<option value="<?php echo $i; ?>"><?php echo $i; ?></option>";
												<?php endfor;?>
											</select>
											<select name="year_input" class="form-control" required>
												<?php for($i = date("Y",strtotime("-3 year")) ; $i < date("Y",strtotime("+2 year")); $i++): ?>
													<option value="<?php echo $i; ?>"<?php echo ($i == date("Y") )?'selected':'';?>><?php echo $i; ?></option>
												<?php endfor;?>
											</select> 
											<button type="submit" class="btn btn-primary">Save</button>
										</span>
									</h4>
								</div>
								<div class="table-responsive">
									<table class="table align-items-center">
										<?php
											$handle = fopen( $uploadedTXT['upload_data']['full_path'],"r" );
											while ( ( $row = fgetcsv( $handle, 10000, " " ) and ( $counter < $maxLines )) != FALSE )
											{
												if( $row['0'] == "Quarter" ){
													continue;
												}
												echo "<tr>";
												foreach ($row as $cell) {
													echo "<td>" . htmlspecialchars($cell) . "</td>";
												}
												echo "</tr>\n";								    
												$counter++;
											}
											fclose($handle);
										?>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
	<?php endif; ?>
	</main>
	<footer class="py-4 bg-light mt-auto">
		<div class="container-fluid">
			<div class="d-flex align-items-center justify-content-between small">
				<div class="text-muted">Copyright &copy; Tiffy's Grade Book <?php echo date('Y'); ?></div>
			</div>
		</div>
	</footer>
</div>