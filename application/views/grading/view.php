<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid">
			<h1 class="mt-4">Results</h1>
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item"><a href="/">Dashboard</a></li>
				<li class="breadcrumb-item"><a href="/grading">Grading</a></li>
				<li class="breadcrumb-item active">View</li>
			</ol>
			<div class="card mb-4">
				<div class="card-body">You can use this page to view all non void results for each student per quarter.</div>
			</div>
			<div class="card mb-4">
				<div class="card-header">Student Grades</div>
				<div class="card-body">
					<div class="table-responsive">
						<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
							<table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
								<thead>
									<tr role="row">
										<th>Student Name</th>
										<th>Student Code</th>
										<th>Quarter</th>
										<th>Year</th>
										<th class="text-right">Average Grade</th>
										<th>hidden_first_name</th>
										<th>hidden_last_name</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach( $results AS $r ): ?>
										<tr>
											<td><?php echo $r->first_name . " " . $r->last_name; ?></td>
											<td><?php echo $r->student_code; ?></td>
											<td>Quarter <?php echo $r->quarter; ?></td>
											<td><?php echo $r->year; ?></td>
											<td class="text-right"><?php echo $r->calculated_grade; ?></td>
											<td><?php echo $r->first_name;?></td>
											<td><?php echo $r->last_name; ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<footer class="py-4 bg-light mt-auto">
		<div class="container-fluid">
			<div class="d-flex align-items-center justify-content-between small">
				<div class="text-muted">Copyright &copy; Tiffy's Grade Book <?php echo date('Y'); ?></div>
			</div>
		</div>
	</footer>
</div>