<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grading extends CI_Controller {

	/**
	 * __construct( )
	 *
	 * @access public
	 */

	public function __construct( )
	{
		parent::__construct( );

		// load model(s)
		$this->load->model( 'grading_model' );
		$this->_data['page_title']= 'Grade App - Student Grading System';
	}

	// ---------------------------------------------------------------------

	/**
	 * index( )
	 *
	 * @access public
	 * @return void
	*/

	public function index()
	{

		if ( isset ( $_FILES['userfile'] ) && !empty ( $_FILES['userfile'] ) ) {

			$config['upload_path'] = 'temp/uploads';
			$config['allowed_types'] = 'txt';
			$config['max_size']	= 1000;
			$config['overwrite'] = true;

			$this->load->library( 'upload', $config );

			if ( !$this->upload->do_upload( ) ) {
				$this->_data['submitError'] = array( 'error' => $this->upload->display_errors( ) );

			}
      		else {
				$uploadData = array( 'upload_data' => $this->upload->data( ) );
				$this->_data['uploadedTXT'] = $uploadData;
     		}

		}

		$this->load->view( 'templates/header', $this->_data );
		$this->load->view( 'grading/index', $this->_data );
		$this->load->view( 'templates/footer', $this->_data );
	}

	// ---------------------------------------------------------------------

	/**
	 * create( )
	 *
	 * @access public
	 * @return void
	*/

	public function create( )
	{
		// quarter and year for these results
		$quarter = $this->input->post( 'quarter_input' );
		$year = $this->input->post( 'year_input' );

		// open the uploaded file
		$handle = fopen( $_POST['hidden_file_name'], "r" );
		while ( ( $row = fgetcsv( $handle, 10000, " " ) ) != FALSE ) {

			// if teacher has left heading in the data, move to the next row
			if( $row['0'] == "Quarter" ) {
				continue;
			}

			// generate student code
			$studentCode = strtoupper( $row['0'] . $row['1'] );
			$firstName = $row['0'];
			$lastName = $row['1'];

			// get student (if it exists)
			$query = $this->grading_model->get_student( $studentCode );

			// student already exists
			if( $query->num_rows( ) > 0 ) {

				foreach( $query->result( ) AS $s ) {
					$student_id = $s->student_id;
				}

				// void any existing records for this quarter / student
				$this->grading_model->void_quarter_tests_by_student( $student_id, $quarter, $year );

			// no record of this student yet
			} else {

				// create student
				$student_id = $this->grading_model->create_student( $firstName, $lastName, $studentCode );

			}

			// loop through the rows and extract the required data
			foreach( $row AS $r ) {			

				// attempting to detect test type ( test / gradingwork )
				if( strlen( $r ) === 1 && ( ctype_alpha( $r ) ) ) {

					// check if we already have a row matching
					$existing_test = $this->grading_model->get_existing_tests( $student_id, $r, $quarter, $year );

					if( $existing_test ) {

						// don't insert a new row, reference the existing one
						$test_id = $existing_test;
					} else {

						// record the current test id
						$test_id = $this->grading_model->insert_test_data( $student_id, $r, $quarter, $year );
					}

					

				// if value is a number then we can assume it's a grade
				} else if( is_numeric( $r ) ) {

					// insert the result for this test
					$this->grading_model->insert_result_data( $student_id, $test_id, $r );
					
				}

			}

		}

		// data has been stored, so lets update the tests table with the average results
		$query = $this->grading_model->get_valid_tests( $quarter, $year );

		foreach( $query AS $q ) {

			// test id and type
			$test_id = $q->test_id;
			$type = $q->type;

			$query = $this->grading_model->get_results( $q->test_id );
			$arr = array_column( $query, "value" );

			//sort the array so smallest result is at the bottom
			rsort( $arr );

			// if this is a gradingwork test, remove the lowest score inline with teachers process - ensuring there is at least one result to process
			if( $type == 'h' && count( $arr ) > 1 ) {
				array_pop( $arr );
			}

			//calculate the average and round to one decimal place
			$array_length = count( $arr );
			$array_sum = array_sum( $arr );
			$average_grade = $array_sum / $array_length;

			$this->grading_model->update_average_grade( $average_grade, $test_id );

		}

		// calculate the final grade
		$this->_calculate_final_grade( );

		redirect( base_url() . 'grading/view');
	}

	// --------------------------------------------------------------------

	/**
	 * view( )
	 *
	 * @access public
	 * @return void
	*/

	public function view()
	{
		$this->_data['js'] = base_url() . "assets/js/grading/view.js";

		// final grade data
		$this->_data['results'] = $this->grading_model->get_final_grades(  );

		$this->load->view( 'templates/header', $this->_data );
		$this->load->view( 'grading/view', $this->_data );
		$this->load->view( 'templates/footer', $this->_data );
	}

	// ---------------------------------------------------------------------

	/**
	 * _calculate_final_grade( )
	 *
	 * @access private
	 * @return void
	*/

	private function _calculate_final_grade()
	{
		$results = $this->grading_model->view_all_results( );

		foreach( $results AS $r ) {

			if( empty( $r->results ) ) {
				continue;
			}

			$homework_grade = 0;
			$test_grade = 0;
			$test_quarter = 0;
			$test_year = 0;
			$test_weight = 0;
			$homework_weight = 0;
			$student_id = 0;

			foreach( $r->results AS $res ) {

				if( $res->type == 'h' ) {
					$homework_grade = $homework_grade + $res->calculated_grade;
					$homework_weight = $res->weighting;
				}

				if( $res->type == 't' ) {
					$test_grade = $test_grade + $res->calculated_grade;
					$test_weight = $res->weighting;
				}

				$test_quarter = $res->quarter;
				$test_year = $res->year;
				$student_id = $res->student_id;				
				
			}

			$test_grade_to_add = ( $test_weight / 100 ) * $test_grade;
			$homework_grade_to_add = ( $homework_weight / 100 ) * $homework_grade;
			$finalGrade = round( $test_grade_to_add + $homework_grade_to_add, 1 );

			// insert final result for this quarter / year
			$this->grading_model->insert_final_grade( $student_id, $test_quarter, $test_year, $finalGrade );

		}

		return $finalGrade;

	}
}
