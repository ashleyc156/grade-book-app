<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * __construct( )
	 *
	 * @access public
	 */

	public function __construct( )
	{
		parent::__construct( );

	}

	// ---------------------------------------------------------------------

	/**
	 * index( )
	 *
	 * @access public
	 * @return void
	*/

	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('home/index');
		$this->load->view('templates/footer');
	}

	// ---------------------------------------------------------------------
}
